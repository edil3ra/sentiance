from django.contrib import admin
from .models import MomentDefinition, Moment

@admin.register(MomentDefinition)
class MomentDefitinionAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Moment)
class MomentAdmin(admin.ModelAdmin):
    list_display = ['start', 'end', 'analysis_type', 'user']
    list_filter = ['start', 'end', 'user']

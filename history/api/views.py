from rest_framework import viewsets
from ..models import Moment, Event, Segment, EventMode
from .serializers import MomentSerializer, EventSerializer, SegmentSerializer, AggTransportSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from django.db.models import F, Sum, Count


class MomentSet(viewsets.ModelViewSet):
    queryset = Moment.objects.all()
    serializer_class = MomentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user']


class EventSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user']


class SegmentSet(viewsets.ModelViewSet):
    queryset = Segment.objects.all()
    serializer_class = SegmentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user']


class TransportSet(viewsets.ViewSet):
    def list(self, request):
        queryset = EventMode.objects.all()

        agg = queryset.values()\
                      .annotate(amount=Count('events__mode'))\
                      .annotate(distance=Sum('events__distance'))\
                      .annotate(duration=Sum(F('events__end') - F('events__start')))\
                      .order_by('-duration')

        serializer = AggTransportSerializer([{
            'mode': item['name'],
            'amount': item['amount'],
            'distance': item['distance'],
            'duration': item['duration']
        } for item in agg], many=True)

        return Response(serializer.data)

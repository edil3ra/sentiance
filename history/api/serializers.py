from rest_framework import serializers
from ..models import MomentDefinition, Moment, Event, EventWaypoint, EventTrajectory, Segment



class MomentDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MomentDefinition
        fields = ['name']


class MomentSerializer(serializers.ModelSerializer):
    moment_definition = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )
    duration = serializers.CharField()

    class Meta:
        model = Moment
        fields = ['user', 'start', 'end', 'analysis_type', 'moment_definition', 'duration']
        read_only_fields = ['duration']
    

class EventWaypointSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventWaypoint
        fields = ['type', 'latitude', 'longitude', 'timestamp', 'accuracy']


class EventTrajectorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EventTrajectory
        fields = ['type', 'encoded']


class EventSerializer(serializers.ModelSerializer):
    type = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )

    mode = serializers.SlugRelatedField(
        read_only=True,
        slug_field='name'
    )
    waypoints = EventWaypointSerializer(many=True, read_only=True)
    trajectory = EventTrajectorySerializer()
    duration = serializers.CharField()

    class Meta:
        model = Event
        fields = [
            'user', 'type', 'start', 'end', 'analysis_type', 'latitude',
            'longitude', 'location', 'mode', 'distance', 'waypoints',
            'trajectory', 'duration'
        ]
        read_only_fields = ['duration']        


class SegmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Segment
        fields = ['name', 'display_name', 'description', 'user']


class AggTransportSerializer(serializers.Serializer):
    mode = serializers.CharField()
    amount = serializers.IntegerField()
    distance = serializers.IntegerField()
    duration = serializers.DurationField()

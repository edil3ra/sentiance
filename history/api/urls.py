from django.urls import path, include
from rest_framework import routers
from . import views


app_name = 'history'


historyRouter = routers.DefaultRouter()
historyRouter.register('moments', views.MomentSet)
historyRouter.register('events', views.EventSet)
historyRouter.register('segments', views.SegmentSet)


urlpatterns = [
    path('', include(historyRouter.urls)),
    path('transport_aggs/', views.TransportSet.as_view({'get': 'list'}))
]

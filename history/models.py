from django.db import models
from django.contrib.auth.models import User


class MomentDefinition(models.Model):
    name = models.CharField(max_length=100)


class Moment(models.Model):
    user = models.ForeignKey(User, related_name='moments', on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    analysis_type = models.CharField(max_length=100)
    moment_definition = models.ForeignKey(
        MomentDefinition,
        on_delete=models.CASCADE,
        related_name='moments'
    )

    @property
    def duration(self):
        return self.end - self.start

    class Meta:
        ordering = ['start']

    def __str__(self):
        return f'start: {self.start}, end: {self.end}'


class EventType(models.Model):
    name = models.CharField(max_length=100)


class EventMode(models.Model):
    name = models.CharField(max_length=100)


class EventLocation(models.Model):
    significance = models.CharField(max_length=100)


class EventTrajectory(models.Model):
    type = models.CharField(max_length=100)
    encoded = models.CharField(max_length=10000)


class Event(models.Model):
    user = models.ForeignKey(User, related_name='events', on_delete=models.CASCADE)
    type = models.ForeignKey(EventType, related_name='events', on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    analysis_type = models.CharField(max_length=100)
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    location = models.ForeignKey(EventLocation, related_name='events', on_delete=models.CASCADE, null=True)
    mode = models.ForeignKey(EventMode, related_name='events', on_delete=models.CASCADE, null=True)
    distance = models.IntegerField(null=True)
    trajectory = models.ForeignKey(EventTrajectory, related_name='events', on_delete=models.CASCADE, null=True)

    @property
    def duration(self):
        return self.end - self.start

    class Meta:
        ordering = ['start']


class EventWaypoint(models.Model):
    event = models.ForeignKey(Event, related_name='waypoints', on_delete=models.CASCADE)
    type = models.CharField(max_length=100)
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    timestamp = models.DateTimeField()
    accuracy = models.IntegerField()


class Segment(models.Model):
    user = models.ManyToManyField(User, related_name='segments')
    name = models.CharField(max_length=100)
    display_name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)

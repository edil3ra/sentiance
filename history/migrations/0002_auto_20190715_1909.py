# Generated by Django 2.2.3 on 2019-07-15 19:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eventtrajectory',
            name='encoded',
            field=models.CharField(max_length=10000),
        ),
    ]

# Generated by Django 2.2.3 on 2019-07-15 19:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0002_auto_20190715_1909'),
    ]

    operations = [
        migrations.RenameField(
            model_name='eventwaypoint',
            old_name='start',
            new_name='timestamp',
        ),
    ]

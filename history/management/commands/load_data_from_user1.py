from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
import json
import os
from ...models import (Moment, MomentDefinition, EventMode, EventType, Event,
                       EventWaypoint, EventTrajectory, EventLocation, Segment)

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class Command(BaseCommand):
    help = 'load initial data from user1.json\n Should only be done once to create fixtures'

    def handle_moments(self, data, user):
        moment_definition_s = set(moment['moment_definition_id']
                                  for moment in data['moment_history'])
        MomentDefinition.objects.bulk_create(
            [MomentDefinition(name=m) for m in moment_definition_s])

        # create moments
        Moment.objects.bulk_create([
            Moment(user=user,
                   start=moment['start'],
                   end=moment['end'],
                   analysis_type=moment['analysis_type'],
                   moment_definition=MomentDefinition.objects.get(
                       name=moment['moment_definition_id']))
            for moment in data['moment_history']
        ])

    def handle_events(self, data, user):
        event_type_s = set(event['type'] for event in data['event_history'])
        EventType.objects.bulk_create(
            [EventType(name=m) for m in event_type_s])

        event_mode_s = set(event['mode'] for event in data['event_history']
                           if 'mode' in event)
        EventMode.objects.bulk_create(
            [EventMode(name=m) for m in event_mode_s])

        event_significance_s = set([
            event['location']['significance']
            for event in data['event_history'] if 'location' in event
        ])
        EventLocation.objects.bulk_create(
            [EventLocation(significance=m) for m in event_significance_s])

        event_trajectories = [
            event['trajectory'] for event in data['event_history']
            if 'trajectory' in event and event.get('trajectory') is not None
        ]
        EventTrajectory.objects.bulk_create(
            [EventTrajectory(**m) for m in event_trajectories])

        # create moments
        events = Event.objects.bulk_create([
            Event(
                user=user,
                type=EventType.objects.filter(name=event.get('type')).first(),
                start=event['start'],
                end=event['end'],
                analysis_type=event['analysis_type'],
                latitude=event.get('latitude'),
                longitude=event.get('longitude'),
                mode=EventMode.objects.filter(name=event.get('mode')).first(),
                location=EventLocation.objects.filter(
                    **event.get('location')).first()
                if event.get('location') else None,
                distance=event.get('distance'),
                trajectory=EventTrajectory.objects.filter(
                    **event.get('trajectory')).first()
                if event.get('trajectory') else None)
            for event in data['event_history']
        ])

        # add waypoints
        event_waypoints = [
            event.get('waypoints', []) for event in data['event_history']
        ]
        for event, waypoints in zip(events, event_waypoints):
            waypoints_models = [
                EventWaypoint(**waypoint, event=event)
                for waypoint in waypoints
            ]
            EventWaypoint.objects.bulk_create(waypoints_models)

    def handle_segments(self, data, user):
        segments = Segment.objects.bulk_create([
            Segment(
                name=segment['segment_definition_id'],
                display_name=segment['segment_definition']['display_name'],
                description=segment['segment_definition']['description'],
            ) for segment in data['segments']
        ])
        user.segments.set(segments)


    def handle(self, *args, **options):
        user = json.load(open(os.path.join(BASE_DIR, './user1.json')))
        data = user['data']['user']

        user = User.objects.create_user(username='user1',
                                        email='user1@gmail.com')
        self.stdout.write(self.style.SUCCESS('user1 created'))

        self.handle_moments(data, user)
        self.stdout.write(self.style.SUCCESS('moments created'))

        self.handle_events(data, user)
        self.stdout.write(self.style.SUCCESS('events created'))

        self.handle_segments(data, user)
        self.stdout.write(self.style.SUCCESS('segments created'))

# Server
## Install dependancies
### Either Create a virtual env
    python3 -m venv /path/to/new/virtual/environment
    pip install -r requirements.txt
### Or add depdenecies inside your pip --user
    pip install --user -r requirements.txt
### Create the database
    CREATE DATABASE sentiance;
    GRANT ALL PRIVILEGES ON DATABASE sentiance TO sentiance;
### Fill database with fixtures
    python manage.py loaddata ./history/fixtures/histories.json

# Client
## Install dependancies
    cd client 
    npm install

# Launch application
## Server
    python manage.py runserver
    server will run at localhost:8000
    
## Client
    cd client 
    npm start
    client will run at localhost:8080
    
    

# Access to api
    "moments": http://localhost:8000/api/moments/,
    "events": http://localhost:8000/api/events/,
    "segments": http://localhost:8000/api/segments/
    "transports_aggs": http://localhost:8000/api/transport_aggs/
    "moments_user": http://localhost:8000/api/moments/?user=1,
    "moments_paginations": http://localhost:8000/api/moments/?limit=100,
    

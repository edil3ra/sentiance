import axios from 'axios'


const HOST = '0.0.0.0'
const PORT = '8000'


const http = axios.create({
  baseURL: `http://${HOST}:${PORT}/api/`
})

function getEvents (params = {}) {
  return http.request({
    method: 'get',
    url: 'events/',
    params: params
  }).then((response) => {
    return response.data
  })
}

function getTransportAggs (params = {}) {
  return http.request({
    method: 'get',
    url: 'transport_aggs/',
    params: params
  }).then((response) => {
    return response.data
  })
}


export default {
  instance: http,
  getEvents,
  getTransportAggs
}

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

import App from './App'
import api from './api'

import 'bulma/css/bulma.min.css'
import 'bulma-timeline/dist/css/bulma-timeline.min.css'


Vue.config.productionTip = false
Vue.prototype.$api = api

Vue.use(VueMoment, {
    moment,
})

Vue.filter('formatHour', function (value) {
  return Number((value).toFixed(2));
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})

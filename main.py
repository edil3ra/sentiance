import django
import os
import json
from datetime import datetime
from django.db.models import Sum, Count, Q
from pg_utils import Seconds
from django.db.models import F, Func

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sentiance.settings')
django.setup()
from django.contrib.auth.models import User

from history.models import (Moment, MomentDefinition, EventMode, EventType,
                            Event, EventWaypoint, EventTrajectory,
                            EventLocation, Segment)

e = Event(user=User.objects.first(),
          type=EventType(name='first'),
          start=datetime.now(),
          end=datetime.now(),
          analysis_type='hello',
          latitude=20.0,
          longitude=20.0)

user = json.load(open('./history/management/commands/user1.json'))
data = user['data']['user']

data.keys()
moment_history = data['moment_history'][0].keys()
event_history = data['event_history'][0].keys()
segments = data['segments'][0].keys()

moment_history = data['moment_history'][1]
event_history = data['event_history'][1]
segments = data['segments']

moment_history
event_history
segments

segments_d = [segment['segment_definition_id'] for segment in data['segments']]

moments_d = set(
    [moment['moment_definition_id'] for moment in data['moment_history']])

events_type = set([event['type'] for event in data['event_history']])

events_location = set([
    event['mode'] for event in data['event_history']
    if event['type'] == 'Transport'
])

events_waypoints = [
    event['waypoints'] for event in data['event_history']
    if event['type'] == 'Transport'
]
events_trajectory = [
    event['trajectory'] for event in data['event_history']
    if event['type'] == 'Transport'
]

event_history = data['event_history']

event1 = data['event_history'][0].keys()
event2 = data['event_history'][355].keys()

event1 = data['event_history'][0]
event2 = data['event_history'][355]

Moment.objects.filter()

transports = dict(EventMode.objects.values_list())

agg = Event.objects.filter(mode__in=list(transports.keys()))\
             .values('mode')\
             .annotate(amount=Count('mode'))\
             .annotate(distance=Sum('distance'))\
             .annotate(duration=Func(F('end'), F('start'), function='age'))\

agg = Event.objects.filter(mode__id__in=list(transports.keys()))\
             .values('mode')\
             .annotate(amount=Count('mode'))\
             .annotate(distance=Sum('distance'))\
             .annotate(duration=Func(F('end'), F('start'), function='age'))\

transports = dict(EventMode.objects.values_list())
agg = Event.objects.filter(mode__in=transports)\
             .values('mode')\
             .annotate(amount=Sum('mode'))

agg = Event.objects.filter(mode!=None)\
             .values('mode')\
             .annotate(amount=Sum('mode'))

a = Event.objects.values('mode').annotate(dcount=Count('mode'))

a = Event.objects.exclude(mode__isnull=True).values('mode').annotate(Count('mode'))

l = Event.objects.exclude(mode__isnull=True)

agg = EventMode.objects.all()\
                       .values()\
                       .annotate(amount=Count('events__mode'))\
                       .annotate(distance=Sum('events__distance'))\
                       .annotate(duration=Sum(F('events__end') - F('events__start')))\
                       .order_by('-duration')


a = EventMode.objects.all().values()\
                       
    




agg_parsed = [{
    'mode': transports[e['mode']],
    'distance': e['distance'],
    'amount': e['amount'],
    'duration': e['duration']
} for e in agg]
